import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    optionRight: null,
    tokenSystem: null,
    nickname: null,
    username: null,
    email: null,
    gender: null,
    accessToken: null,
    showOptionsCenter: null,
    showMenuMobile: false
  },
  mutations: {
    setOptionRight (optionRight, value){
      this.state.optionRight = value
    },
    setShowOptionsCenter (showOptionsCenter, value){
      this.state.showOptionsCenter = value
    },
    setTokenSystem (tokenSystem, value){
      this.state.tokenSystem = value
    },
    setNickname (nickname, value){
      this.state.nickname = value
    },
    setUsername (username, value){
      this.state.username = value
    },
    setEmail (email, value){
      this.state.email = value
    },
    setGender (gender, value){
      if(value == 'male') this.state.gender = 'M'
      else this.state.gender = 'F'
    },
    setAccessToken (accessToken, value){
      this.state.accessToken = value
    },
    setShowMenuMobile (showMenuMobile, value){
      this.state.showMenuMobile = value
    }
  },
  getters: {
    getUsername: state => {
      return state.username
    },
    getAccessToken: state => {
      return state.accessToken
    },
    getNickname: state => {
      return state.nickname
    },
    userLogued: state => {
      return state.tokenSystem == null ? false : true
    }
  },
  plugins: [new VuexPersistence().plugin]
})
